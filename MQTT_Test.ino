// WiFi & Connectivity Includes
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// WiFi & Connectivity Settings
const char* wifi_ssid = "Internet";
const char* wifi_pass = "xxx";
const char* mqtt_srvr = "192.168.0.54";
const char* mqtt_user = "mcutest";
const char* mqtt_pass = "xxx";
const char* mqtt_topic_test = "test/hello";

WiFiClient espClient;
PubSubClient client(espClient);

void setup() {
  Serial.begin(115200);
  setup_wifi();

  client.setServer(mqtt_srvr, 1883);
  client.setCallback(callback);
  setup_mqtt();

  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
}

void setup_wifi() {
  delay(10);
  Serial.println();
  Serial.print("WiFi Connecting to ");
  Serial.println(wifi_ssid);

  WiFi.begin(wifi_ssid, wifi_pass);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void setup_mqtt() {
  Serial.println();
  Serial.print("MQTT Connecting to ");
  Serial.println(mqtt_srvr);
  while (!client.connected()) {
    if (client.connect("MCUTestClient", mqtt_user, mqtt_pass)) {
      Serial.println("MQTT Connected");
      client.subscribe(mqtt_topic_test);
    } else {
      Serial.print("MQTT Failed, rc=");
      Serial.print(client.state());
      Serial.println(". Tring again in 5 seconds...");
      delay(5000);
    }
  }
}

void loop() {
  // put your main code here, to run repeatedly:
  if (!client.connected()) {
    setup_mqtt();
  }

  client.loop();
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  payload[length] = '\0';
  if (strcmp((char*)payload, "on") == 0) {
    digitalWrite(LED_BUILTIN, LOW);
  } else if (strcmp((char*)payload, "off") == 0) {
    digitalWrite(LED_BUILTIN, HIGH);
  }
}

